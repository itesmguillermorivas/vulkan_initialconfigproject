#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <iostream>
#include <vector>
#include <cstring>
// 4 - vamos a usar el tipo optional 
#include <optional>
#include <set>
#include <cstdint> 
#include <algorithm>


// definición de constantes de tamaño de ventana
const uint32_t WIDTH = 800;
const uint32_t HEIGHT = 600;

// CAPAS DE VALIDACION
// en vulkan es configurable el nivel de validación con el que queremos trabajar
// verificación de errores

// definiendo un vector que contenga las capas que queremos validar
const std::vector<const char*> validationLayers = {"VK_LAYER_KHRONOS_validation"};

// 23 - let's add an extension vector 
// we're going be using this to declare our chain swap
const std::vector<const char*> deviceExtensions = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };


// dejar la posibilidad de revisar capas o no 
#ifdef NDEBUG
    const bool enableValidationLayers = false;
#else
    const bool enableValidationLayers = true;
#endif

// 6 - vamos a declarar un struct que va a contener una referencia a las familias de colas que nos importan
struct QueueFamilyIndices {

    std::optional<uint32_t> graphicsFamily;
    
    // 18 - add a second index for the presentation queue family
    std::optional<uint32_t> presentFamily;

    // 7 - método de conveniencia para verificar que todas las family queues tienen valor
    bool isComplete() {
        return graphicsFamily.has_value() && presentFamily.has_value();
    }
};

// 25 - we need to do more than just check for chain swap support. We need to setup the chain swap 
// and save several references that will be used later
struct SwapChainSupportDetails {
    VkSurfaceCapabilitiesKHR capabilities;
    std::vector<VkSurfaceFormatKHR> formats;
    std::vector<VkPresentModeKHR> presentModes;
};


// how to define a class in C++
// can be done in a single file 
// can be separated in header + definition

class FirstVulkanExample {

// in a c++ class we classify first by access modifier
public:

    void run(){
        // fun stuff here later!
        initWindow();
        initVulkan();
        mainLoop();
        cleanup();
    }

private:

    // podemos declarar atributos aquí
    // lo primerito - mi objeto ventana
    GLFWwindow* window;

    // necesitamos crear una instancia de vulkan
    // vamos creando primero el objeto donde va a residir
    VkInstance instance;

    // 2 - necesitamos una instancia que guarde referencia al device físico que vamos a utilizar en nuestra app vulkan
    // importante: esta referencia se limpia al destruir la instancia de app de vulkan
    VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;

    // 8 - agregar un logical device - puede haber n por dispositivo físico
    VkDevice device; 

    // 11 - En el momento de creación de disposítivo lógico se crea también el graphics queue
    VkQueue graphicsQueue;

    // 13 - Vamos a crear la surface 
    // surface - espacio donde se va a presentar el render
    VkSurfaceKHR surface;

    // 17 - agregar una referencia para trabajo con la presentation queue
    VkQueue presentQueue;

    // 33 - create an instance to save our newly created swap chain
    VkSwapchainKHR swapChain;

    // 35 - declare the images that will be used by the swap chain
    std::vector<VkImage> swapChainImages;

    // 37 - vamos a guardar referencia al formato y extent que finalmente quedaron como resultado
    VkFormat swapChainImageFormat;
    VkExtent2D swapChainExtent;

    // luego los métodos

    void initWindow() {

        glfwInit();

        // poner parametros para creacion de ventanas
        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);


        window = glfwCreateWindow(WIDTH, HEIGHT, "Vulkancito", nullptr, nullptr);

    }

    void initVulkan() {

        // primero va a estar chequeo de validación por capas
        if(enableValidationLayers && !checkValidationLayerSupport()){
            throw std::runtime_error("se solicitaron capas pero no estan disponibles!");
        }

        // aquí van a estar varios procesos relacionados al setup de vulkan - 2don creacion de instancia
        createInstance();

        // 14 - creamos surface
        createSurface();

        // 1 - una vez que la instancia fue creada seleccionar dispositivo físico con el que vamos a interactuar
        pickPhysicalDevice();

        // 31 - método para creacion de swap chain
        createSwapChain();

        // 9 - una vez creado y validado el dispositivo físico creamos el lógico
        createLogicalDevice();



        // 11 - Crear surface - superficie donde vamos a estar dibujando
        // vamos a usar una extensión de vulkan - VK_KHR_surface para interactuar con una ventana
        VkSurfaceKHR surface;

    }

    // 32 - implementación de método
    void createSwapChain(){

        SwapChainSupportDetails swapChainSupport = querySwapChainSupport(physicalDevice);

        // retrieve the 3 values we just made methods for
        VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(swapChainSupport.formats);
        VkPresentModeKHR presentMode = chooseSwapPresentMode(swapChainSupport.presentModes);
        VkExtent2D extent = chooseSwapExtent(swapChainSupport.capabilities);


        // establish the minimum amount of images within the swap chain
        uint32_t imageCount = swapChainSupport.capabilities.minImageCount + 1;

        // setup max images
        if(swapChainSupport.capabilities.maxImageCount > 0 && imageCount > swapChainSupport.capabilities.maxImageCount){
            imageCount = swapChainSupport.capabilities.maxImageCount;
        }

        VkSwapchainCreateInfoKHR createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
        createInfo.surface = surface;
        createInfo.minImageCount = imageCount;
        createInfo.imageFormat = surfaceFormat.format;
        createInfo.imageColorSpace = surfaceFormat.colorSpace;
        createInfo.imageExtent = extent;
        createInfo.imageArrayLayers = 1;
        createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;


        // get queue families and determine ownership of images in the swap chain
        QueueFamilyIndices indices = findQueueFamilies(physicalDevice);
        uint32_t queueFamilyIndices[] = {indices.graphicsFamily.value(), indices.presentFamily.value()};

        // 2 possibilites
        // they're the same family or not
        if(indices.graphicsFamily != indices.presentFamily){

            createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
            createInfo.queueFamilyIndexCount = 2;
            createInfo.pQueueFamilyIndices = queueFamilyIndices;
        } else {

            createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
            createInfo.queueFamilyIndexCount = 0;
            createInfo.pQueueFamilyIndices = nullptr;
        }

        // apply transformation already done to the world
        createInfo.preTransform = swapChainSupport.capabilities.currentTransform;

        // is there a need to blend?
        createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
        createInfo.presentMode = presentMode;
        createInfo.clipped = VK_TRUE;
        createInfo.oldSwapchain = VK_NULL_HANDLE;

        if(vkCreateSwapchainKHR(device, &createInfo, nullptr, &swapChain) != VK_SUCCESS){
            throw std::runtime_error("no se pudo crear swapchain");
        }

        // 36 - guardar referencias a imagenes que se van a utilizar
        vkGetSwapchainImagesKHR(device, swapChain, &imageCount, nullptr);
        swapChainImages.resize(imageCount);
        vkGetSwapchainImagesKHR(device, swapChain, &imageCount, swapChainImages.data());

        // 38 - una vez declarados los atributos los guardamos
        swapChainImageFormat = surfaceFormat.format;
        swapChainExtent = extent;
    }

    // 15 - implementación de método
    void createSurface() {
        if(glfwCreateWindowSurface(instance, window, nullptr, &surface) != VK_SUCCESS){
            throw std::runtime_error("problemas en creacion de surface");
        }
    }

    void createLogicalDevice() {

        QueueFamilyIndices indices = findQueueFamilies(physicalDevice);

        // 20 - changes made in order to consider several queues instead of just one
        std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
        std::set<uint32_t> uniqueQueueFamilies = {indices.graphicsFamily.value(), indices.presentFamily.value()};

        float queuePriority = 1.0f;
        for(uint32_t queueFamily : uniqueQueueFamilies){

            VkDeviceQueueCreateInfo queueCreateInfo{};
            queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            queueCreateInfo.queueFamilyIndex = queueFamily;
            queueCreateInfo.queueCount = 1;
            queueCreateInfo.pQueuePriorities = &queuePriority;
            
            // push the newly create VkDeviceQueueCreateInfo structure into the vector
            queueCreateInfos.push_back(queueCreateInfo);

        }

        // this was changed aftewards to add logic to consider the presentation queue
        /*
        // empezamos a crear structs con info para finalmente crear el dispositivo lógico
        VkDeviceQueueCreateInfo queueCreateInfo{};
        queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueCreateInfo.queueFamilyIndex = indices.graphicsFamily.value();
        queueCreateInfo.queueCount = 1;

        // en vulkan hay que asignar una prioridad a las queues
        float queuePriority = 1.0f;
        queueCreateInfo.pQueuePriorities = &queuePriority;
        */

        // objeto que define requisitos de physical device (ahorita no le pondremos nada especial)
        VkPhysicalDeviceFeatures deviceFeatures{};

        VkDeviceCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;

        // 21 - more changes considering the newly created queue vector
        /*
        createInfo.pQueueCreateInfos = &queueCreateInfo;
        createInfo.queueCreateInfoCount = 1;
        */
        createInfo.pQueueCreateInfos = queueCreateInfos.data();
        createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());

        createInfo.pEnabledFeatures = &deviceFeatures;

        // 24 - modify create info to consider extension support in the logical device
        createInfo.enabledExtensionCount = static_cast<uint32_t>(deviceExtensions.size());
        createInfo.ppEnabledExtensionNames = deviceExtensions.data();
        
        // agregamos layers a validar en dispositivo lógico si es algo que nos interesa
        if(enableValidationLayers){

            createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
            createInfo.ppEnabledLayerNames = validationLayers.data();
        } else {

            createInfo.enabledLayerCount = 0;
        }


        // ahora sí toca crear el dispositivo lógico
        if(vkCreateDevice(physicalDevice, &createInfo, nullptr, &device) != VK_SUCCESS){
            throw std::runtime_error("fallas en creacion de dispositivo lógico");
        }

        // 12 - obtener referencia al graphics queue para utilizar en el futuro
        vkGetDeviceQueue(device, indices.graphicsFamily.value(), 0, &graphicsQueue);

        // 22 - same as we did with the graphics queue we will retrieve the reference
        // for the presentation queue
        vkGetDeviceQueue(device, indices.presentFamily.value(), 0, &presentQueue);
    }

    // 26 - implement method to return populated chain swap detail struct 
    SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device){
        SwapChainSupportDetails details;

        // 3 values to retrieve
        // first - surface capabilites
        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &details.capabilities);

        // check for formats 
        uint32_t formatCount;
        vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, nullptr);

        if(formatCount > 0){

            // first - resize vector to fit all information
            details.formats.resize(formatCount);
            vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, details.formats.data());
        }

        // finally check for presentation modes
        uint32_t presentModeCount;
        vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, nullptr);

        if(presentModeCount > 0){

            details.presentModes.resize(presentModeCount);
            vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, details.presentModes.data());
        }

        return details;
    }

    // 28 - we need to pick 3 values
    // surface format - color
    // presentation mode - how to swap the images in the swap chain
    // swap extent - resolution that will be used to display the render

    // first lets get the surface format available
    VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats) {

        // iterate and search for the best
        for(const auto& availableFormat : availableFormats){
            if(availableFormat.format == VK_FORMAT_B8G8R8_SRGB && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR){
                return availableFormat;
            }
        }

        // if that guy was not available return the first one
        return availableFormats[0];
    }

    // 29 - presentation mode
    // VK_PRESENT_MODE_IMMEDIATE_KHR 
    // VK_PRESENT_MODE_FIFO_KHR
    // VK_PRESENT_MODE_FIFO_RELAXED_KHR
    // VK_PRESENT_MODE_MAILBOX_KHR
    VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes){

        for(const auto& availablePresentMode : availablePresentModes){
            if(availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR){
                return availablePresentMode;
            }
        }

        return VK_PRESENT_MODE_FIFO_KHR;
    }

    // 30 - swap extent 
    // extent - alcance - tamaño de render en pantalla
    VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilites){

        if(capabilites.currentExtent.width != UINT32_MAX){
            return capabilites.currentExtent;
        } else {

            int width, height;
            glfwGetFramebufferSize(window, &width, &height);

            VkExtent2D actualExtent = {
                static_cast<uint32_t>(width),
                static_cast<uint32_t>(height)
            };

            actualExtent.width = std::clamp(actualExtent.width, capabilites.minImageExtent.width, capabilites.maxImageExtent.width);
            actualExtent.height = std::clamp(actualExtent.height, capabilites.minImageExtent.height, capabilites.maxImageExtent.height);
            
            return actualExtent;
        }
    }

    void pickPhysicalDevice() {

        // buscar tarjeta de video capaz de correr la instancia de app de vulkan ya configurada 
        // vamos a usar la primera que encuentre

        // 3 - enumerar cantidad de dispositivos disponibles
        uint32_t deviceCount = 0;

        // lo primero es obtener la cantidad de dispositivos disponibles
        vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr);

        // si no hay dispositivos mandamos una excepción
        if(deviceCount == 0){
            throw std::runtime_error("no tienes dispositivos físicos capaces de correr vulkan!");
        }

        // si llegamos aquí sí hubo al menos 1 dispositivo físico disponible
        // hay que guardar referencia al device
        std::vector<VkPhysicalDevice> devices(deviceCount);
        vkEnumeratePhysicalDevices(instance, &deviceCount, devices.data());

        // recorrer devices y guardar el primero que sea apto de acuerdo a nuestras necesidades
        // (nuestras necesidades serán descritas más adelante)
        for(const auto& device : devices){

            // si el dispositivo es funcional de acuerdo a lo que queremos lo guardamos en la referencia
            // del objeto y terminamos el loop
            if(isDeviceSuitable(device)){
                physicalDevice = device;
                break;
            }
        }


        // si llegamos aquí y physicalDevice sigue siendo VK_NULL_HANDLE
        // significa que ningún dispositivo cumple con las condiciones
        if(physicalDevice == VK_NULL_HANDLE){
            throw std::runtime_error("ninguno de tus GPUs cumple los requisitos");
        }
    }

    bool isDeviceSuitable(VkPhysicalDevice device){

        // 7 - una vez creado el struct podemos modificar el método
        QueueFamilyIndices indices = findQueueFamilies(device);

        // 19 - Check for support for all of our extensions
        bool extensionsSupported = checkDeviceExtensionSupport(device);

        // 27 - check support for swapchains
        bool swapChainAdequate = false;
        if(extensionsSupported){

            SwapChainSupportDetails swapChainSupport = querySwapChainSupport(device);
            swapChainAdequate = !swapChainSupport.formats.empty() && !swapChainSupport.presentModes.empty();
        }

        return indices.isComplete() && extensionsSupported && swapChainAdequate;
    }

    // 20 - implementation for the extension support check method
    bool checkDeviceExtensionSupport(VkPhysicalDevice device){

        uint32_t extensionCount;
        vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);

        std::vector<VkExtensionProperties> availableExtensions(extensionCount);
        vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availableExtensions.data());

        std::set<std::string> requiredExtensions(deviceExtensions.begin(), deviceExtensions.end()); 

        // iterate through the extensions found on the physical device
        for(const auto& extension : availableExtensions){

            requiredExtensions.erase(extension.extensionName);
        }

        return requiredExtensions.empty();
    }

    // 5 - queues - colas 
    // todas las acciones que solicitemos a GPU por medio del api vulkan se agregan a una cola
    // las colas pertenecen a familias que tienen características / capacidades particulares
    // es necesario que verifiquemos la capacidad de nuestro GPU vs nuestras expectativas
    QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device) {
        
        // declaramos una referencia del tipo del struct para regresar como resultado
        QueueFamilyIndices indices;

        uint32_t queueFamilyCount = 0;

        vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);

        std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
        vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());

        int i = 0;
        for(const auto& queueFamily : queueFamilies){

            if(queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT){
                
                indices.graphicsFamily = i;
            }

            // 19 - check for the presentation queue family
            VkBool32 presentSupport = false;
            vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &presentSupport);

            if(presentSupport){

                indices.presentFamily = i;
            }

            // si todas las familias estuvieron presentes ya acabamos
            if(indices.isComplete()){
                break;
            }

            i++;
        }

        return indices;
    }

    // este metodo exclusivamente será utilizado para la creación de la instancia
    void createInstance(){

        // paradigma muy común en vulkan - utilizar un struct con valores para crear una instancia de algo
        VkApplicationInfo info{};

        // rellenar de argumentos
        // valores generales de aplicacion
        info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        info.pApplicationName = "Vulkan 101";
        info.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
        info.pEngineName = "Ninguno";
        info.engineVersion = VK_MAKE_VERSION(1, 0, 0);
        info.apiVersion = VK_API_VERSION_1_0;

        // variables a ser utilizadas al obtener extensiones de gflw
        // queremos que la instancia de app de vulkan sea capaz de interactuar con glfw
        uint32_t glfwExtensionCount = 0;
        const char** glfwExtensions;

        glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

        // con el anterior creamos OTRO objeto con datos
        VkInstanceCreateInfo createInfo {};
        createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        createInfo.pApplicationInfo = &info;
        createInfo.enabledExtensionCount = glfwExtensionCount;
        createInfo.ppEnabledExtensionNames = glfwExtensions;
       
        // AGREGADO POSTERIORMENTE - agregar a instancia validación de capas
        if(enableValidationLayers){

            createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
            createInfo.ppEnabledLayerNames = validationLayers.data();
        } else {

            createInfo.enabledLayerCount = 0;
        }

        // ahora sí va la llamada a la creación de instancia de app de vulkan
        // opcional - utilizar objeto result para checar resultados
        VkResult result = vkCreateInstance(&createInfo, nullptr, &instance);

    }

    bool checkValidationLayerSupport() {

        // regresa si es posible la validación de las capas que definimos en el vector de capas a checar
        uint32_t layerCount;
        
        // obtenemos la cantidad de layers de validacion disponibles en este sistema
        vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

        // creamos vector que contenga strings que definan las layers que son
        std::vector<VkLayerProperties> availableLayers(layerCount);

        // invocamos el método nuevamente, esta vez llena el vector con la info correspondiente
        vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

        // chequeo - capas deseadas vs capas disponibles

        // loop externo - capas que desearía que existieran
        for(const char * layerName : validationLayers){

            bool layerFound = false;

            // loop interno - capas que sé que tengo disponibles
            for (const auto& layerProperties : availableLayers) {

                // comparamos strings entre la del vector de deseados y el de disponibles
                if(strcmp(layerName, layerProperties.layerName) == 0){

                    layerFound = true;
                    break;
                }
            }

            if(!layerFound){
                return false;
            }

        }

        return true;

    }

    void mainLoop() {

        // crear loop de glfw
        while(!glfwWindowShouldClose(window)){
            glfwPollEvents();
        }
    }

    // en algunos ejemplos / implementaciones se usa un destructor en lugar de este método
    // destructor - método que se manda llamar al terminar un objeto (NO TENEMOS UNO AQUI!)
    // destructor normalmente se usa para liberar apuntadores internos del objeto
    void cleanup() {

        // very important - we DON'T have a garbage collector so we need to clean up
        // limpieza de glfw
        glfwDestroyWindow(window);
        glfwTerminate();

        // 34 - destroy swapchain we created
        vkDestroySwapchainKHR(device, swapChain, nullptr);

        // 10 - destruimos el dispositivo lógico
        vkDestroyDevice(device, nullptr);

        // 14 - limpieza de surface
        vkDestroySurfaceKHR(instance, surface, nullptr);

        // limpieza de instancia de vulkan
        vkDestroyInstance(instance, nullptr);

        
    }

};

int main() {

    // como en java lo que corre es lo que esté en el método main
    FirstVulkanExample app;

    try {

        std::cout << "hola amiguitos!" << std::endl;
        app.run();
    } catch(const std::exception& e){
        
        // cerr - salida para errores
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}