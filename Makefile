CFLAGS = -std=c++17 -O2
LDFLAGS = -lglfw3dll -lvulkan-1

VulkanTest: main.cpp
	g++ $(CFLAGS) -o VulkanTest main.cpp $(LDFLAGS)

.PHONY: test clean

test: VulkanTest
	./VulkanTest

clean:
	rm -f VulkanTest