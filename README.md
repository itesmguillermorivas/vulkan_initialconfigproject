# PASOS PARA LA CREACION DE UN PROYECTO "COMPLETO" EN VULKAN

- creación de ventana con GLFW
- creación de instancia de Vulkan
    - determinar extensiones necesarias (recuerda concepto de extensión)
- determinar capas de validación
- obtener referencia a dispositivo físico
    - determinar si dispositivo físico es compatible con las familias de colas que vamos a requerir
- crear dispositivo lógico
- obtener referencia al graphics queue del dispositivo lógico recién creado
- creación de objeto surface para render posterior
- creación de referencia para presentation queue
- búsqueda de soporte en dispositivo físico para presentatión queue
- create the chain swap - first declare extensions to use
- modify create info to consider extension support in the logical device
- setup values for swap chain
- create images to be used on swap chain
- left at 38